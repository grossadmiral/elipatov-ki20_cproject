#ifndef SEARCHDIALOG_H
#define SEARCHDIALOG_H

#include <QWidget>

namespace Ui {
class SearchDialog;
}

class SearchDialog : public QWidget
{
    Q_OBJECT

public:
    explicit SearchDialog(QWidget *parent = nullptr);
    ~SearchDialog();

private slots:
    void on_Back_Button_clicked();

private:
    Ui::SearchDialog *ui;
};

#endif // SEARCHDIALOG_H
