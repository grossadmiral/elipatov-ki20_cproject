#ifndef TREE_H
#define TREE_H
#include <QString>
#include "man.h"

class Tree
{
public:
    Tree();
    void setOwner_name(QString new_owner_name);
    QString getOwner_name();
    void setRelatives(Man newRelative);
    Man getRelatives();

    void save_mans();

    std::vector <Man> *getMans(QString);

    void addMan(Man&, QString);

    bool searchMan(int& id_);

    Man *getCurMan();

private:
    QString m_owner_name;
    std::vector<Man> m_peoples;
    void load_mans(QString);
    Man cur_mans;
};

#endif // TREE_H
