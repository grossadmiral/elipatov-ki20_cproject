#ifndef USER_H
#define USER_H
#include <QString>

class User
{
public:
    User();

    QString getName();
    void setName(QString newName);
    QString getPassword();
    void setPassword(QString newPassword);
    int getRole();
    void setRole(int newRole);

    void load(QDataStream &ist);
    void save(QDataStream &ost) const;

private:
    QString m_name;
    QString m_password;
    int m_role;
};

inline QDataStream &operator<<(QDataStream &ost, const User &u)
{
    u.save(ost);
    return ost;
}

inline QDataStream &operator>>(QDataStream &ist, User &u)
{
    u.load(ist);
    return ist;
}


#endif // USER_H
