#ifndef AUTHORISATIONDIALOG_H
#define AUTHORISATIONDIALOG_H

#include <QWidget>
#include "userdb.h"

namespace Ui {
class AuthorisationDialog;
}

class AuthorisationDialog : public QWidget
{
    Q_OBJECT

public:
    explicit AuthorisationDialog(QWidget *parent = nullptr);
    ~AuthorisationDialog();
    bool CheckAuthCorrectness(QString login, QString password);

private slots:
    void on_Exit_Button_clicked();

    void on_Accept_Button_clicked();

private:
    void openWorkWindow(int, QString);

    Ui::AuthorisationDialog *ui;
    UserDB userdatabese;
    std::vector <User>* users;
};

#endif // AUTHORISATIONDIALOG_H
