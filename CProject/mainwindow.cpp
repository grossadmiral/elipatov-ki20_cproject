#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "authorisationdialog.h"
#include "searchdialog.h"
#include "editrelativesdialog.h"
#include "tree.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::setDB(QString login_){
    people = treedatabase.getMans(login_);
    setPeopleText();
}

void MainWindow::setPeopleText(){
    QString PeopleText = "Имя ID Пол ID Матери Id Отеца : ";
    for (size_t i = 0; i < people->size(); i++) {
        PeopleText += QString(people->at(i).getName());
        PeopleText += " ";
        PeopleText += QString("%1").arg(people->at(i).getID());
        PeopleText += " ";
        PeopleText += QString("%1").arg(people->at(i).getgender());
        PeopleText += " ";
        PeopleText += QString("%1").arg(people->at(i).getMotherID());
        PeopleText += " ";
        PeopleText += QString("%1").arg(people->at(i).getfathertID());
        PeopleText += "  /  ";
    }
    ui->Tree_View->setText(PeopleText);
}

bool CheckRequest(QString Query){
    throw std::runtime_error("not implemented");
}


void MainWindow::on_ProgramExit_Button_clicked()
{
    close();
}


void MainWindow::on_User_Exit_clicked()
{
    AuthorisationDialog * AuthorisationWindow = new AuthorisationDialog();
    AuthorisationWindow->show();
    close();
}


void MainWindow::on_Search_Button_clicked()
{
    SearchDialog* SearchWinow = new SearchDialog();
    SearchWinow->show();
}


void MainWindow::on_TreeEdit_Button_clicked()
{
    EditRelativesDialog* EditRelativesWindow = new EditRelativesDialog();
    EditRelativesWindow->show();
}

