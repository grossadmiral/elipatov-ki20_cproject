#ifndef USERDB_H
#define USERDB_H
#include "user.h"

class UserDB
{
public:
    UserDB();
    int CheckAuth(QString login, QString password);
    //void AddUser(QString login, QString password);
    void EditUser(QString login, QString password);
    void DeleteUser(QString login, QString password);

    void setUser(User newUser);
    User getUser();

    void save_users();


    std::vector <User> *getUsers();

    void addUser(User&);

    bool searchUser(QString& login_, QString& password_);

    User *getCurUser();

private:
    std::vector<User> m_users;
    void load_users();
    User cur_user;
};

#endif // USERDB_H
