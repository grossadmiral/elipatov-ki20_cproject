#include "user.h"
#include <sstream>

User::User()
{

}

QString User::getName(){
    return m_name;
}


void User::setName(QString newName){
    m_name = newName;
}


QString User::getPassword(){
    return m_password;
}


void User::setPassword(QString newPassword){
    m_password = newPassword;
}


int User::getRole(){
    return m_role;
}


void User::setRole(int newRole){
    m_role = newRole;
}

void User::load(QDataStream &ist) {
    QString temp_role;
    ist >> m_name >> m_password >> temp_role;
    m_role = temp_role.toUInt();
}

void User::save(QDataStream &ost) const{
    ost << m_name << m_password << QString("%1").arg(m_role);
}

