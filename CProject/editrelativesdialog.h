#ifndef EDITRELATIVESDIALOG_H
#define EDITRELATIVESDIALOG_H

#include <QWidget>
#include "tree.h"

namespace Ui {
class EditRelativesDialog;
}

class EditRelativesDialog : public QWidget
{
    Q_OBJECT

public:
    explicit EditRelativesDialog(QWidget *parent = nullptr);
    ~EditRelativesDialog();
    bool CheckRelativesFields(QString login, QString password);

    void setERDB(Tree&treedatabase_, QString login_);

private slots:
    void on_Back_Button_clicked();

    void on_AddReletives_Button_clicked();

    void on_DeleteReletives_Button_clicked();

private:
    Ui::EditRelativesDialog *ui;
    QString login;
    std::vector<Man>* people;
    Tree treedatabase;

};

#endif // EDITRELATIVESDIALOG_H
