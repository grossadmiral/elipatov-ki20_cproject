#ifndef TREEDB_H
#define TREEDB_H
#include "tree.h"

class TreeDB
{
public:
    TreeDB();
    bool RelativeCheck(int relativeID);
    void RelativeAdd(int relativeID, QString Name, int fathertID, int MotherID, bool gender);
    void RelativeDelete(int relativeID);
    void GetInfo(QString Query);

    void setTree(Tree new_tree);
    Tree getTree();

private:
    std::vector<Tree> m_trees;
};

#endif // TREEDB_H
