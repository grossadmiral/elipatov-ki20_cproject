#include "editrelativesdialog.h"
#include "ui_editrelativesdialog.h"

#include "mainwindow.h"

EditRelativesDialog::EditRelativesDialog(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::EditRelativesDialog)
{
    ui->setupUi(this);
}

EditRelativesDialog::~EditRelativesDialog()
{
    delete ui;
}


void EditRelativesDialog::setERDB(Tree &treedatabase_, QString login_){
    treedatabase = treedatabase_;
    people = treedatabase.getMans(login_);
    login = login_;
}

bool CheckRelativesFields(QString login, QString password){
    throw std::runtime_error("not implemented");
}


void EditRelativesDialog::on_Back_Button_clicked()
{
    close();
}


void EditRelativesDialog::on_AddReletives_Button_clicked()
{
    QString n_name = ui->NameEnter_Field->text();
    QString n_fatherid = ui->FatherEnter_Field->text();
    QString n_motherid = ui->MotherEnter_Field->text();
    int n_id = 0;
    QString n_gender = ui->GenderEnter_Field->text();
    Man new_man;
    new_man.setName(n_name);
    new_man.setMotherID(n_motherid.toUInt());
    new_man.setfathertID(n_fatherid.toUInt());
    new_man.setgender(n_gender.toUInt());
    new_man.setID(n_id);
    people->push_back(new_man);

    MainWindow *treewiewvindow = new MainWindow;
    treewiewvindow->setDB(treedatabase, login);
    treewiewvindow->show();
    close();
}


void EditRelativesDialog::on_DeleteReletives_Button_clicked()
{
    close();
}

