#include "authorisationdialog.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    AuthorisationDialog w;
    w.show();
    return a.exec();
}
