#ifndef ADMINISTRATORDIALOG_H
#define ADMINISTRATORDIALOG_H

#include <QMainWindow>
#include "userdb.h"

namespace Ui {
class AdministratorDialog;
}

class AdministratorDialog : public QMainWindow
{
    Q_OBJECT

public:
    explicit AdministratorDialog(QWidget *parent = nullptr);
    ~AdministratorDialog();

    void setDB(UserDB&);

    void setUsersText();

private slots:
    void on_ProgramExit_Button_clicked();

    void on_User_Exit_clicked();

    void on_UserEdit_Button_clicked();

private:
    UserDB userdatabese;
    Ui::AdministratorDialog *ui;
    std::vector <User>* users;
};

#endif // ADMINISTRATORDIALOG_H
