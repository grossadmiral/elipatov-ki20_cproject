QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    administratordialog.cpp \
    authorisationdialog.cpp \
    editrelativesdialog.cpp \
    edituserdialog.cpp \
    main.cpp \
    mainwindow.cpp \
    man.cpp \
    searchdialog.cpp \
    tree.cpp \
    treedb.cpp \
    user.cpp \
    userdb.cpp

HEADERS += \
    administratordialog.h \
    authorisationdialog.h \
    editrelativesdialog.h \
    edituserdialog.h \
    mainwindow.h \
    man.h \
    searchdialog.h \
    tree.h \
    treedb.h \
    user.h \
    userdb.h

FORMS += \
    administratordialog.ui \
    authorisationdialog.ui \
    editrelativesdialog.ui \
    edituserdialog.ui \
    mainwindow.ui \
    searchdialog.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DISTFILES += \
    DB/TreeCSV.csv \
    DB/UsersCSV.csv

SUBDIRS += \
    Test/Test.pro \
    Test/Test.pro \
    Test/Test.pro
