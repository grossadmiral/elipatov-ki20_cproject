#include "tree.h"

#include <QFile>
#include <QSaveFile>

Tree::Tree()
{

}

void Tree::setOwner_name(QString new_owner_name){
    m_owner_name = new_owner_name;
}


QString Tree::getOwner_name(){
    return m_owner_name;
}


void Tree::setRelatives(Man newRelative){

}


Man Tree::getRelatives(){

}

void Tree::save_mans()
{
    QFile outf("users.tnb");
    outf.open(QIODevice::WriteOnly);
    QDataStream ost(&outf);
    for (size_t i = 0; i < m_peoples.size(); i++) {
        ost << m_peoples[i];
    }
    outf.close();
}

void Tree::addMan(Man& user_, QString login_){
    QString file_name = login_ + ".tnb";
    QFile outf(file_name);
    outf.open(QIODevice::Append);
    QDataStream out(&outf);
    out << user_;
}

void Tree::load_mans(QString login_){
    QString file_name = login_ + ".tnb";
    QFile inf(file_name);
    inf.open(QIODevice::ReadOnly);
    QDataStream ist(&inf);
    m_peoples.clear();
    while (!ist.atEnd())
    {
        Man u;
        ist >> u;
        m_peoples.push_back(u);
    }

    if (m_peoples.size() == 0)
    {
        Man m;
        int id = 0;
        QString name = login_;
        int fatherid = -1;
        int motherid = -1;
        int gender = 1;
        m.setID(id);
        m.setName(name);
        m.setMotherID(motherid);
        m.setfathertID(fatherid);
        m.setgender(gender);
        m_peoples.push_back(m);
        addMan(m, login_);
    }
}

std::vector <Man> *Tree::getMans(QString login_)
{
    if (m_peoples.empty())
    {
        load_mans(login_);
    }
    return &m_peoples;
}


bool Tree::searchMan(int& id_)
{
    for (size_t i = 0; i < m_peoples.size(); i++) {
        if (m_peoples[i].getID() == id_) {
            cur_mans = m_peoples[i];
            return true;
        }
    }
    return false;
}

Man *Tree::getCurMan()
{
    return &cur_mans;
}

