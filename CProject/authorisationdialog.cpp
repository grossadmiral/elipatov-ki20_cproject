#include "authorisationdialog.h"
#include "ui_authorisationdialog.h"

#include "mainwindow.h"
#include "administratordialog.h"

#include "QMessageBox"

AuthorisationDialog::AuthorisationDialog(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::AuthorisationDialog)
{
    ui->setupUi(this);
    users = userdatabese.getUsers();
}

AuthorisationDialog::~AuthorisationDialog()
{
    delete ui;
}

bool CheckAuthCorrectness(QString login, QString password){
    throw std::runtime_error("not implemented");
}


void AuthorisationDialog::on_Exit_Button_clicked()
{
    close();
}


void AuthorisationDialog::on_Accept_Button_clicked()
{
    QString login = ui->Login_Enter_Field->text();
    QString password = ui->Password_Enter_Field->text();
    if (login.isEmpty()) {
        QMessageBox::warning(0, "Ошибка", "Поле логин пустое.");
        return;
    }
    else if (password.isEmpty()) {
        QMessageBox::warning(0, "Ошибка", "Поле пароль пустое.");
        return;
    }

    if (!userdatabese.searchUser(login, password)) {
        QMessageBox::warning(0, "Ошибка", "Пользователя с введенным логином и паролем не найден.");
        return;
    }

    openWorkWindow(userdatabese.getCurUser()->getRole(), login);
}

void AuthorisationDialog::openWorkWindow(int role_, QString login)
{
    if (role_ == 0 || role_ == 1) {
        MainWindow *treewiewwindow = new MainWindow;
        treewiewwindow->setDB(login);
        treewiewwindow->show();
        on_Exit_Button_clicked();
    }
    if (role_ == 1) {
        AdministratorDialog *usereditwindow = new AdministratorDialog;
        usereditwindow->setDB(userdatabese);
        usereditwindow->show();
        on_Exit_Button_clicked();
    }
}

