#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "tree.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    bool CheckRequest(QString Query);

    void setDB(QString);

    void setPeopleText();

private slots:
    void on_ProgramExit_Button_clicked();

    void on_User_Exit_clicked();

    void on_Search_Button_clicked();

    void on_TreeEdit_Button_clicked();

private:
    Ui::MainWindow *ui;
    std::vector<Man>* people;
    Tree treedatabase;
};
#endif // MAINWINDOW_H
