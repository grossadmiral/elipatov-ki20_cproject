#include "man.h"
#include <sstream>

Man::Man()
{

}

void Man::setName(QString new_name){
    m_name = new_name;
}


QString Man::getName(){
    return m_name;
}


void Man::setID(int new_ID){
    m_id = new_ID;
}


int Man::getID(){
    return m_id;
}


void Man::setfathertID(int new_fathertID){
    m_fatherid = new_fathertID;
}


int Man::getfathertID(){
    return m_fatherid;
}


void Man::setMotherID(int new_motherid){
    m_motherid = new_motherid;
}


int Man::getMotherID(){
    return m_motherid;
}


void Man::setgender(bool new_gender){
    m_gender = new_gender;
}


int Man::getgender(){
    return m_gender;
}

void Man::load(QDataStream &ist) {

    QString temp_id = "";
    QString temp_gender = "";
    QString temp_motherid = "";
    QString temp_fatherid = "";

    ist >> temp_id >> m_name >> temp_gender >> temp_motherid >> temp_fatherid;

    m_id = temp_id.toUInt();
    m_gender = temp_gender.toUInt();
    m_motherid = temp_motherid.toUInt();
    m_fatherid = temp_fatherid.toUInt();

}

void Man::save(QDataStream &ost) const{
     ost << QString("%1").arg(m_id) << m_name << QString("%1").arg(m_gender) << QString("%1").arg(m_motherid) << QString("%1").arg(m_fatherid);
}
