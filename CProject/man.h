#ifndef MAN_H
#define MAN_H
#include <QString>
#include <string>

class Man
{
public:
    Man();

    void setName(QString new_name);
    QString getName();
    void setID(int new_ID);
    int getID();
    void setfathertID(int new_fathertID);
    int getfathertID();
    void setMotherID(int new_motherID);
    int getMotherID();
    void setgender(bool new_gender);
    int getgender();

    void load(QDataStream &ist);
    void save(QDataStream &ost) const;

private:
    QString m_name;
    int m_id;
    int m_fatherid;
    int m_motherid;
    int m_gender;
};

inline QDataStream &operator<<(QDataStream &ost, const Man &m)
{
    m.save(ost);
    return ost;
}

inline QDataStream &operator>>(QDataStream &ist, Man &m)
{
    m.load(ist);
    return ist;
}

#endif // MAN_H
