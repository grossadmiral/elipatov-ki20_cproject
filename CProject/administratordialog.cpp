#include "administratordialog.h"
#include "ui_administratordialog.h"

#include "authorisationdialog.h"
#include "edituserdialog.h"

AdministratorDialog::AdministratorDialog(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::AdministratorDialog)
{
    ui->setupUi(this);
}

AdministratorDialog::~AdministratorDialog()
{
    delete ui;
}

void AdministratorDialog::setDB(UserDB& userdatabese_){
    userdatabese = userdatabese_;
    users = userdatabese.getUsers();
    setUsersText();
}

void AdministratorDialog::setUsersText(){
    QString UserText = "Логин Пароль Роль : ";
    for (size_t i = 0; i < users->size(); i++) {
        UserText += QString(users->at(i).getName());
        UserText += " ";
        UserText += QString(users->at(i).getPassword());
        UserText += " ";
        UserText += QString("%1").arg(users->at(i).getRole());
        UserText += " / ";
    }
    ui->UserView->setText(UserText);
}

void AdministratorDialog::on_ProgramExit_Button_clicked()
{
    userdatabese.save_users();
    close();
}


void AdministratorDialog::on_User_Exit_clicked()
{
    AuthorisationDialog * window1 = new AuthorisationDialog();
    window1->show();
    close();
}


void AdministratorDialog::on_UserEdit_Button_clicked()
{
    EditUserDialog *usereditwindow = new EditUserDialog;
    usereditwindow->setEUDB(userdatabese);
    usereditwindow->show();
    on_ProgramExit_Button_clicked();
}
