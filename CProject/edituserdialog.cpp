#include "edituserdialog.h"
#include "ui_edituserdialog.h"

#include "administratordialog.h"

EditUserDialog::EditUserDialog(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::EditUserDialog)
{
    ui->setupUi(this);
}

EditUserDialog::~EditUserDialog()
{
    delete ui;
}


void EditUserDialog::setEUDB(UserDB& userdatabese_){
    userdatabese = userdatabese_;
    users = userdatabese.getUsers();
}

bool CheckUserFields(QString login, QString password){
    throw std::runtime_error("not implemented");
}


void EditUserDialog::on_Back_Button_clicked()
{
    close();
}


void EditUserDialog::on_AddUser_Button_clicked()
{
    QString n_name = ui->LoginEnter_Field->text();
    QString n_password = ui->PasswordEnter_Field->text();
    QString n_role = ui->AccesEnter_Field->text();
    User new_user;
    new_user.setName(n_name);
    new_user.setPassword(n_password);
    new_user.setRole(n_role.toUInt());
    users->push_back(new_user);

    AdministratorDialog *usereditwindow = new AdministratorDialog;
    usereditwindow->setDB(userdatabese);
    usereditwindow->show();
    close();
}


void EditUserDialog::on_DeleteUser_Button_clicked()
{
    close();
}


void EditUserDialog::on_SaveUser_Button_clicked()
{
    close();
}

