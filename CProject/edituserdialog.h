#ifndef EDITUSERDIALOG_H
#define EDITUSERDIALOG_H

#include <QWidget>
#include "userdb.h"

namespace Ui {
class EditUserDialog;
}

class EditUserDialog : public QWidget
{
    Q_OBJECT

public:
    explicit EditUserDialog(QWidget *parent = nullptr);
    ~EditUserDialog();
    bool CheckUserFields(QString login, QString password);

    void setEUDB(UserDB&);

private slots:
    void on_Back_Button_clicked();

    void on_AddUser_Button_clicked();

    void on_DeleteUser_Button_clicked();

    void on_SaveUser_Button_clicked();

private:
    Ui::EditUserDialog *ui;
    UserDB userdatabese;
    std::vector <User>* users;
};

#endif // EDITUSERDIALOG_H
