/********************************************************************************
** Form generated from reading UI file 'editrelativesdialog.ui'
**
** Created by: Qt User Interface Compiler version 6.1.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_EDITRELATIVESDIALOG_H
#define UI_EDITRELATIVESDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_EditRelativesDialog
{
public:
    QWidget *verticalLayoutWidget;
    QVBoxLayout *Main_Groupe;
    QLabel *Main_Label;
    QHBoxLayout *RelativeMainInfo_Groupe;
    QVBoxLayout *FullName_Groupe;
    QHBoxLayout *Name_Groupe;
    QLabel *Name_Label;
    QLineEdit *NameEnter_Field;
    QHBoxLayout *Surname_Groupe;
    QLabel *Surname_Label;
    QLineEdit *SurnameEnter_Field;
    QHBoxLayout *Patronymic_Groupe;
    QLabel *Patronymic_Label;
    QLineEdit *PatronymicEnter_Field;
    QVBoxLayout *RelativesInfo_Groupe;
    QHBoxLayout *Father_Groupe;
    QLabel *Father_Label;
    QLineEdit *FatherEnter_Field;
    QHBoxLayout *Mother_Groupe;
    QLabel *Mother_Label;
    QLineEdit *MotherEnter_Field;
    QHBoxLayout *MainGender_Group;
    QLabel *Gender_Label;
    QLineEdit *GenderEnter_Field;
    QSpacerItem *horizontalSpacer_4;
    QHBoxLayout *Action_Groupe;
    QPushButton *AddReletives_Button;
    QPushButton *DeleteReletives_Button;
    QHBoxLayout *back_Group;
    QSpacerItem *horizontalSpacer;
    QPushButton *Back_Button;

    void setupUi(QWidget *EditRelativesDialog)
    {
        if (EditRelativesDialog->objectName().isEmpty())
            EditRelativesDialog->setObjectName(QString::fromUtf8("EditRelativesDialog"));
        EditRelativesDialog->resize(719, 306);
        verticalLayoutWidget = new QWidget(EditRelativesDialog);
        verticalLayoutWidget->setObjectName(QString::fromUtf8("verticalLayoutWidget"));
        verticalLayoutWidget->setGeometry(QRect(20, 12, 677, 271));
        Main_Groupe = new QVBoxLayout(verticalLayoutWidget);
        Main_Groupe->setObjectName(QString::fromUtf8("Main_Groupe"));
        Main_Groupe->setContentsMargins(0, 0, 0, 0);
        Main_Label = new QLabel(verticalLayoutWidget);
        Main_Label->setObjectName(QString::fromUtf8("Main_Label"));

        Main_Groupe->addWidget(Main_Label);

        RelativeMainInfo_Groupe = new QHBoxLayout();
        RelativeMainInfo_Groupe->setObjectName(QString::fromUtf8("RelativeMainInfo_Groupe"));
        FullName_Groupe = new QVBoxLayout();
        FullName_Groupe->setObjectName(QString::fromUtf8("FullName_Groupe"));
        Name_Groupe = new QHBoxLayout();
        Name_Groupe->setObjectName(QString::fromUtf8("Name_Groupe"));
        Name_Label = new QLabel(verticalLayoutWidget);
        Name_Label->setObjectName(QString::fromUtf8("Name_Label"));

        Name_Groupe->addWidget(Name_Label);

        NameEnter_Field = new QLineEdit(verticalLayoutWidget);
        NameEnter_Field->setObjectName(QString::fromUtf8("NameEnter_Field"));

        Name_Groupe->addWidget(NameEnter_Field);


        FullName_Groupe->addLayout(Name_Groupe);

        Surname_Groupe = new QHBoxLayout();
        Surname_Groupe->setObjectName(QString::fromUtf8("Surname_Groupe"));
        Surname_Label = new QLabel(verticalLayoutWidget);
        Surname_Label->setObjectName(QString::fromUtf8("Surname_Label"));

        Surname_Groupe->addWidget(Surname_Label);

        SurnameEnter_Field = new QLineEdit(verticalLayoutWidget);
        SurnameEnter_Field->setObjectName(QString::fromUtf8("SurnameEnter_Field"));

        Surname_Groupe->addWidget(SurnameEnter_Field);


        FullName_Groupe->addLayout(Surname_Groupe);

        Patronymic_Groupe = new QHBoxLayout();
        Patronymic_Groupe->setObjectName(QString::fromUtf8("Patronymic_Groupe"));
        Patronymic_Label = new QLabel(verticalLayoutWidget);
        Patronymic_Label->setObjectName(QString::fromUtf8("Patronymic_Label"));

        Patronymic_Groupe->addWidget(Patronymic_Label);

        PatronymicEnter_Field = new QLineEdit(verticalLayoutWidget);
        PatronymicEnter_Field->setObjectName(QString::fromUtf8("PatronymicEnter_Field"));

        Patronymic_Groupe->addWidget(PatronymicEnter_Field);


        FullName_Groupe->addLayout(Patronymic_Groupe);


        RelativeMainInfo_Groupe->addLayout(FullName_Groupe);

        RelativesInfo_Groupe = new QVBoxLayout();
        RelativesInfo_Groupe->setObjectName(QString::fromUtf8("RelativesInfo_Groupe"));
        Father_Groupe = new QHBoxLayout();
        Father_Groupe->setObjectName(QString::fromUtf8("Father_Groupe"));
        Father_Label = new QLabel(verticalLayoutWidget);
        Father_Label->setObjectName(QString::fromUtf8("Father_Label"));

        Father_Groupe->addWidget(Father_Label);

        FatherEnter_Field = new QLineEdit(verticalLayoutWidget);
        FatherEnter_Field->setObjectName(QString::fromUtf8("FatherEnter_Field"));

        Father_Groupe->addWidget(FatherEnter_Field);


        RelativesInfo_Groupe->addLayout(Father_Groupe);

        Mother_Groupe = new QHBoxLayout();
        Mother_Groupe->setObjectName(QString::fromUtf8("Mother_Groupe"));
        Mother_Label = new QLabel(verticalLayoutWidget);
        Mother_Label->setObjectName(QString::fromUtf8("Mother_Label"));

        Mother_Groupe->addWidget(Mother_Label);

        MotherEnter_Field = new QLineEdit(verticalLayoutWidget);
        MotherEnter_Field->setObjectName(QString::fromUtf8("MotherEnter_Field"));

        Mother_Groupe->addWidget(MotherEnter_Field);


        RelativesInfo_Groupe->addLayout(Mother_Groupe);


        RelativeMainInfo_Groupe->addLayout(RelativesInfo_Groupe);


        Main_Groupe->addLayout(RelativeMainInfo_Groupe);

        MainGender_Group = new QHBoxLayout();
        MainGender_Group->setObjectName(QString::fromUtf8("MainGender_Group"));
        Gender_Label = new QLabel(verticalLayoutWidget);
        Gender_Label->setObjectName(QString::fromUtf8("Gender_Label"));

        MainGender_Group->addWidget(Gender_Label);

        GenderEnter_Field = new QLineEdit(verticalLayoutWidget);
        GenderEnter_Field->setObjectName(QString::fromUtf8("GenderEnter_Field"));

        MainGender_Group->addWidget(GenderEnter_Field);

        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        MainGender_Group->addItem(horizontalSpacer_4);


        Main_Groupe->addLayout(MainGender_Group);

        Action_Groupe = new QHBoxLayout();
        Action_Groupe->setObjectName(QString::fromUtf8("Action_Groupe"));
        AddReletives_Button = new QPushButton(verticalLayoutWidget);
        AddReletives_Button->setObjectName(QString::fromUtf8("AddReletives_Button"));

        Action_Groupe->addWidget(AddReletives_Button);

        DeleteReletives_Button = new QPushButton(verticalLayoutWidget);
        DeleteReletives_Button->setObjectName(QString::fromUtf8("DeleteReletives_Button"));

        Action_Groupe->addWidget(DeleteReletives_Button);


        Main_Groupe->addLayout(Action_Groupe);

        back_Group = new QHBoxLayout();
        back_Group->setObjectName(QString::fromUtf8("back_Group"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        back_Group->addItem(horizontalSpacer);

        Back_Button = new QPushButton(verticalLayoutWidget);
        Back_Button->setObjectName(QString::fromUtf8("Back_Button"));

        back_Group->addWidget(Back_Button);


        Main_Groupe->addLayout(back_Group);


        retranslateUi(EditRelativesDialog);

        QMetaObject::connectSlotsByName(EditRelativesDialog);
    } // setupUi

    void retranslateUi(QWidget *EditRelativesDialog)
    {
        EditRelativesDialog->setWindowTitle(QCoreApplication::translate("EditRelativesDialog", "Form", nullptr));
        Main_Label->setText(QCoreApplication::translate("EditRelativesDialog", "\320\237\320\260\321\200\320\260\320\274\320\265\321\202\321\200\321\213 \321\200\320\276\320\264\321\201\321\202\320\262\320\265\320\275\320\275\320\270\320\272\320\260:", nullptr));
        Name_Label->setText(QCoreApplication::translate("EditRelativesDialog", "\320\230\320\274\321\217:", nullptr));
        Surname_Label->setText(QCoreApplication::translate("EditRelativesDialog", "\320\244\320\260\320\274\320\270\320\273\320\270\321\217:", nullptr));
        Patronymic_Label->setText(QCoreApplication::translate("EditRelativesDialog", "\320\236\321\202\321\207\320\265\321\201\321\202\320\262\320\276:", nullptr));
        Father_Label->setText(QCoreApplication::translate("EditRelativesDialog", "\320\230\320\275\320\264\320\265\320\272\321\201 \320\236\321\202\321\206\320\260:", nullptr));
        Mother_Label->setText(QCoreApplication::translate("EditRelativesDialog", "\320\230\320\275\320\264\320\265\320\272\321\201 \320\234\320\260\321\202\320\265\321\200\320\270:", nullptr));
        Gender_Label->setText(QCoreApplication::translate("EditRelativesDialog", "\320\237\320\276\320\273:", nullptr));
        AddReletives_Button->setText(QCoreApplication::translate("EditRelativesDialog", "\320\224\320\276\320\261\320\260\320\262\320\270\321\202\321\214", nullptr));
        DeleteReletives_Button->setText(QCoreApplication::translate("EditRelativesDialog", "\320\243\320\264\320\260\320\273\320\270\321\202\321\214", nullptr));
        Back_Button->setText(QCoreApplication::translate("EditRelativesDialog", "\320\235\320\260\320\267\320\260\320\264", nullptr));
    } // retranslateUi

};

namespace Ui {
    class EditRelativesDialog: public Ui_EditRelativesDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_EDITRELATIVESDIALOG_H
