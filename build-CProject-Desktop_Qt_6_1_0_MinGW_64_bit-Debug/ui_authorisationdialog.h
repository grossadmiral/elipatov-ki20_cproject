/********************************************************************************
** Form generated from reading UI file 'authorisationdialog.ui'
**
** Created by: Qt User Interface Compiler version 6.1.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_AUTHORISATIONDIALOG_H
#define UI_AUTHORISATIONDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_AuthorisationDialog
{
public:
    QWidget *horizontalLayoutWidget;
    QHBoxLayout *Login_Groupe;
    QLabel *Login_Label;
    QLineEdit *Login_Enter_Field;
    QWidget *horizontalLayoutWidget_2;
    QHBoxLayout *Password_Group;
    QLabel *Passwword_Label;
    QLineEdit *Password_Enter_Field;
    QWidget *horizontalLayoutWidget_3;
    QHBoxLayout *Button_Groupe;
    QSpacerItem *horizontalSpacer;
    QPushButton *Accept_Button;
    QPushButton *Exit_Button;

    void setupUi(QWidget *AuthorisationDialog)
    {
        if (AuthorisationDialog->objectName().isEmpty())
            AuthorisationDialog->setObjectName(QString::fromUtf8("AuthorisationDialog"));
        AuthorisationDialog->resize(400, 205);
        horizontalLayoutWidget = new QWidget(AuthorisationDialog);
        horizontalLayoutWidget->setObjectName(QString::fromUtf8("horizontalLayoutWidget"));
        horizontalLayoutWidget->setGeometry(QRect(10, 30, 371, 41));
        Login_Groupe = new QHBoxLayout(horizontalLayoutWidget);
        Login_Groupe->setObjectName(QString::fromUtf8("Login_Groupe"));
        Login_Groupe->setContentsMargins(0, 0, 0, 0);
        Login_Label = new QLabel(horizontalLayoutWidget);
        Login_Label->setObjectName(QString::fromUtf8("Login_Label"));

        Login_Groupe->addWidget(Login_Label);

        Login_Enter_Field = new QLineEdit(horizontalLayoutWidget);
        Login_Enter_Field->setObjectName(QString::fromUtf8("Login_Enter_Field"));

        Login_Groupe->addWidget(Login_Enter_Field);

        horizontalLayoutWidget_2 = new QWidget(AuthorisationDialog);
        horizontalLayoutWidget_2->setObjectName(QString::fromUtf8("horizontalLayoutWidget_2"));
        horizontalLayoutWidget_2->setGeometry(QRect(10, 100, 371, 41));
        Password_Group = new QHBoxLayout(horizontalLayoutWidget_2);
        Password_Group->setObjectName(QString::fromUtf8("Password_Group"));
        Password_Group->setContentsMargins(0, 0, 0, 0);
        Passwword_Label = new QLabel(horizontalLayoutWidget_2);
        Passwword_Label->setObjectName(QString::fromUtf8("Passwword_Label"));

        Password_Group->addWidget(Passwword_Label);

        Password_Enter_Field = new QLineEdit(horizontalLayoutWidget_2);
        Password_Enter_Field->setObjectName(QString::fromUtf8("Password_Enter_Field"));

        Password_Group->addWidget(Password_Enter_Field);

        horizontalLayoutWidget_3 = new QWidget(AuthorisationDialog);
        horizontalLayoutWidget_3->setObjectName(QString::fromUtf8("horizontalLayoutWidget_3"));
        horizontalLayoutWidget_3->setGeometry(QRect(10, 160, 371, 23));
        Button_Groupe = new QHBoxLayout(horizontalLayoutWidget_3);
        Button_Groupe->setObjectName(QString::fromUtf8("Button_Groupe"));
        Button_Groupe->setContentsMargins(0, 0, 0, 0);
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        Button_Groupe->addItem(horizontalSpacer);

        Accept_Button = new QPushButton(horizontalLayoutWidget_3);
        Accept_Button->setObjectName(QString::fromUtf8("Accept_Button"));

        Button_Groupe->addWidget(Accept_Button);

        Exit_Button = new QPushButton(horizontalLayoutWidget_3);
        Exit_Button->setObjectName(QString::fromUtf8("Exit_Button"));

        Button_Groupe->addWidget(Exit_Button);


        retranslateUi(AuthorisationDialog);

        QMetaObject::connectSlotsByName(AuthorisationDialog);
    } // setupUi

    void retranslateUi(QWidget *AuthorisationDialog)
    {
        AuthorisationDialog->setWindowTitle(QCoreApplication::translate("AuthorisationDialog", "Form", nullptr));
        Login_Label->setText(QCoreApplication::translate("AuthorisationDialog", "\320\233\320\276\320\263\320\270\320\275:", nullptr));
        Passwword_Label->setText(QCoreApplication::translate("AuthorisationDialog", "\320\237\320\260\321\200\320\276\320\273\321\214:", nullptr));
        Accept_Button->setText(QCoreApplication::translate("AuthorisationDialog", "\320\237\320\276\320\264\321\202\320\262\320\265\321\200\320\264\320\270\321\202\321\214", nullptr));
        Exit_Button->setText(QCoreApplication::translate("AuthorisationDialog", "\320\222\321\213\320\271\321\202\320\270", nullptr));
    } // retranslateUi

};

namespace Ui {
    class AuthorisationDialog: public Ui_AuthorisationDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_AUTHORISATIONDIALOG_H
