/********************************************************************************
** Form generated from reading UI file 'edituserdialog.ui'
**
** Created by: Qt User Interface Compiler version 6.1.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_EDITUSERDIALOG_H
#define UI_EDITUSERDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_EditUserDialog
{
public:
    QWidget *verticalLayoutWidget;
    QVBoxLayout *Main_Groupe;
    QLabel *Main_Label;
    QHBoxLayout *Login_Groupe;
    QLabel *Login_Label;
    QLineEdit *LoginEnter_Field;
    QSpacerItem *horizontalSpacer_2;
    QHBoxLayout *Password_Groupe;
    QLabel *Password_Label;
    QLineEdit *PasswordEnter_Field;
    QSpacerItem *horizontalSpacer_3;
    QHBoxLayout *MainAccess_Group;
    QLabel *Acces_Label;
    QLineEdit *AccesEnter_Field;
    QSpacerItem *horizontalSpacer_4;
    QHBoxLayout *Action_Groupe;
    QPushButton *AddUser_Button;
    QPushButton *DeleteUser_Button;
    QPushButton *SaveUser_Button;
    QHBoxLayout *back_Group;
    QSpacerItem *horizontalSpacer;
    QPushButton *Back_Button;

    void setupUi(QWidget *EditUserDialog)
    {
        if (EditUserDialog->objectName().isEmpty())
            EditUserDialog->setObjectName(QString::fromUtf8("EditUserDialog"));
        EditUserDialog->resize(674, 343);
        verticalLayoutWidget = new QWidget(EditUserDialog);
        verticalLayoutWidget->setObjectName(QString::fromUtf8("verticalLayoutWidget"));
        verticalLayoutWidget->setGeometry(QRect(30, 20, 611, 283));
        Main_Groupe = new QVBoxLayout(verticalLayoutWidget);
        Main_Groupe->setObjectName(QString::fromUtf8("Main_Groupe"));
        Main_Groupe->setContentsMargins(0, 0, 0, 0);
        Main_Label = new QLabel(verticalLayoutWidget);
        Main_Label->setObjectName(QString::fromUtf8("Main_Label"));

        Main_Groupe->addWidget(Main_Label);

        Login_Groupe = new QHBoxLayout();
        Login_Groupe->setObjectName(QString::fromUtf8("Login_Groupe"));
        Login_Label = new QLabel(verticalLayoutWidget);
        Login_Label->setObjectName(QString::fromUtf8("Login_Label"));

        Login_Groupe->addWidget(Login_Label);

        LoginEnter_Field = new QLineEdit(verticalLayoutWidget);
        LoginEnter_Field->setObjectName(QString::fromUtf8("LoginEnter_Field"));

        Login_Groupe->addWidget(LoginEnter_Field);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        Login_Groupe->addItem(horizontalSpacer_2);


        Main_Groupe->addLayout(Login_Groupe);

        Password_Groupe = new QHBoxLayout();
        Password_Groupe->setObjectName(QString::fromUtf8("Password_Groupe"));
        Password_Label = new QLabel(verticalLayoutWidget);
        Password_Label->setObjectName(QString::fromUtf8("Password_Label"));

        Password_Groupe->addWidget(Password_Label);

        PasswordEnter_Field = new QLineEdit(verticalLayoutWidget);
        PasswordEnter_Field->setObjectName(QString::fromUtf8("PasswordEnter_Field"));

        Password_Groupe->addWidget(PasswordEnter_Field);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        Password_Groupe->addItem(horizontalSpacer_3);


        Main_Groupe->addLayout(Password_Groupe);

        MainAccess_Group = new QHBoxLayout();
        MainAccess_Group->setObjectName(QString::fromUtf8("MainAccess_Group"));
        Acces_Label = new QLabel(verticalLayoutWidget);
        Acces_Label->setObjectName(QString::fromUtf8("Acces_Label"));

        MainAccess_Group->addWidget(Acces_Label);

        AccesEnter_Field = new QLineEdit(verticalLayoutWidget);
        AccesEnter_Field->setObjectName(QString::fromUtf8("AccesEnter_Field"));

        MainAccess_Group->addWidget(AccesEnter_Field);

        horizontalSpacer_4 = new QSpacerItem(58, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        MainAccess_Group->addItem(horizontalSpacer_4);


        Main_Groupe->addLayout(MainAccess_Group);

        Action_Groupe = new QHBoxLayout();
        Action_Groupe->setObjectName(QString::fromUtf8("Action_Groupe"));
        AddUser_Button = new QPushButton(verticalLayoutWidget);
        AddUser_Button->setObjectName(QString::fromUtf8("AddUser_Button"));

        Action_Groupe->addWidget(AddUser_Button);

        DeleteUser_Button = new QPushButton(verticalLayoutWidget);
        DeleteUser_Button->setObjectName(QString::fromUtf8("DeleteUser_Button"));

        Action_Groupe->addWidget(DeleteUser_Button);

        SaveUser_Button = new QPushButton(verticalLayoutWidget);
        SaveUser_Button->setObjectName(QString::fromUtf8("SaveUser_Button"));

        Action_Groupe->addWidget(SaveUser_Button);


        Main_Groupe->addLayout(Action_Groupe);

        back_Group = new QHBoxLayout();
        back_Group->setObjectName(QString::fromUtf8("back_Group"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        back_Group->addItem(horizontalSpacer);

        Back_Button = new QPushButton(verticalLayoutWidget);
        Back_Button->setObjectName(QString::fromUtf8("Back_Button"));

        back_Group->addWidget(Back_Button);


        Main_Groupe->addLayout(back_Group);


        retranslateUi(EditUserDialog);

        QMetaObject::connectSlotsByName(EditUserDialog);
    } // setupUi

    void retranslateUi(QWidget *EditUserDialog)
    {
        EditUserDialog->setWindowTitle(QCoreApplication::translate("EditUserDialog", "Form", nullptr));
        Main_Label->setText(QCoreApplication::translate("EditUserDialog", "\320\237\320\260\321\200\320\260\320\274\320\265\321\202\321\200\321\213 \320\277\320\276\320\273\321\214\320\267\320\276\320\262\320\260\321\202\320\265\320\273\321\217:", nullptr));
        Login_Label->setText(QCoreApplication::translate("EditUserDialog", "\320\233\320\276\320\263\320\270\320\275:", nullptr));
        Password_Label->setText(QCoreApplication::translate("EditUserDialog", "\320\237\320\260\321\200\320\276\320\273\321\214:", nullptr));
        Acces_Label->setText(QCoreApplication::translate("EditUserDialog", "\320\237\321\200\320\260\320\262\320\260:", nullptr));
        AddUser_Button->setText(QCoreApplication::translate("EditUserDialog", "\320\224\320\276\320\261\320\260\320\262\320\270\321\202\321\214", nullptr));
        DeleteUser_Button->setText(QCoreApplication::translate("EditUserDialog", "\320\243\320\264\320\260\320\273\320\270\321\202\321\214", nullptr));
        SaveUser_Button->setText(QCoreApplication::translate("EditUserDialog", "\320\241\320\276\321\205\321\200\320\260\320\275\320\270\321\202\321\214", nullptr));
        Back_Button->setText(QCoreApplication::translate("EditUserDialog", "\320\235\320\260\320\267\320\260\320\264", nullptr));
    } // retranslateUi

};

namespace Ui {
    class EditUserDialog: public Ui_EditUserDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_EDITUSERDIALOG_H
