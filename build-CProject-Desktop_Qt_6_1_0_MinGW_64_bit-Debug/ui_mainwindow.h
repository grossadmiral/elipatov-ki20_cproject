/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 6.1.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *action_2;
    QAction *action_3;
    QAction *action_4;
    QWidget *centralwidget;
    QWidget *horizontalLayoutWidget;
    QHBoxLayout *Search_groupe;
    QLineEdit *Search_Field;
    QPushButton *Search_Button;
    QWidget *horizontalLayoutWidget_2;
    QHBoxLayout *Exit_Groupe;
    QSpacerItem *horizontalSpacer;
    QPushButton *ProgramExit_Button;
    QWidget *horizontalLayoutWidget_3;
    QHBoxLayout *TreeEdit_Groupe;
    QSpacerItem *horizontalSpacer_2;
    QPushButton *TreeEdit_Button;
    QLabel *Tree_Label;
    QLineEdit *Tree_View;
    QMenuBar *menubar;
    QMenu *menu;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(823, 485);
        action_2 = new QAction(MainWindow);
        action_2->setObjectName(QString::fromUtf8("action_2"));
        action_3 = new QAction(MainWindow);
        action_3->setObjectName(QString::fromUtf8("action_3"));
        action_4 = new QAction(MainWindow);
        action_4->setObjectName(QString::fromUtf8("action_4"));
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        horizontalLayoutWidget = new QWidget(centralwidget);
        horizontalLayoutWidget->setObjectName(QString::fromUtf8("horizontalLayoutWidget"));
        horizontalLayoutWidget->setGeometry(QRect(21, 10, 779, 31));
        Search_groupe = new QHBoxLayout(horizontalLayoutWidget);
        Search_groupe->setObjectName(QString::fromUtf8("Search_groupe"));
        Search_groupe->setContentsMargins(0, 0, 0, 0);
        Search_Field = new QLineEdit(horizontalLayoutWidget);
        Search_Field->setObjectName(QString::fromUtf8("Search_Field"));

        Search_groupe->addWidget(Search_Field);

        Search_Button = new QPushButton(horizontalLayoutWidget);
        Search_Button->setObjectName(QString::fromUtf8("Search_Button"));

        Search_groupe->addWidget(Search_Button);

        horizontalLayoutWidget_2 = new QWidget(centralwidget);
        horizontalLayoutWidget_2->setObjectName(QString::fromUtf8("horizontalLayoutWidget_2"));
        horizontalLayoutWidget_2->setGeometry(QRect(20, 400, 779, 25));
        Exit_Groupe = new QHBoxLayout(horizontalLayoutWidget_2);
        Exit_Groupe->setObjectName(QString::fromUtf8("Exit_Groupe"));
        Exit_Groupe->setContentsMargins(0, 0, 0, 0);
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        Exit_Groupe->addItem(horizontalSpacer);

        ProgramExit_Button = new QPushButton(horizontalLayoutWidget_2);
        ProgramExit_Button->setObjectName(QString::fromUtf8("ProgramExit_Button"));

        Exit_Groupe->addWidget(ProgramExit_Button);

        horizontalLayoutWidget_3 = new QWidget(centralwidget);
        horizontalLayoutWidget_3->setObjectName(QString::fromUtf8("horizontalLayoutWidget_3"));
        horizontalLayoutWidget_3->setGeometry(QRect(20, 360, 781, 31));
        TreeEdit_Groupe = new QHBoxLayout(horizontalLayoutWidget_3);
        TreeEdit_Groupe->setObjectName(QString::fromUtf8("TreeEdit_Groupe"));
        TreeEdit_Groupe->setContentsMargins(0, 0, 0, 0);
        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        TreeEdit_Groupe->addItem(horizontalSpacer_2);

        TreeEdit_Button = new QPushButton(horizontalLayoutWidget_3);
        TreeEdit_Button->setObjectName(QString::fromUtf8("TreeEdit_Button"));

        TreeEdit_Groupe->addWidget(TreeEdit_Button);

        Tree_Label = new QLabel(centralwidget);
        Tree_Label->setObjectName(QString::fromUtf8("Tree_Label"));
        Tree_Label->setGeometry(QRect(20, 50, 777, 21));
        Tree_View = new QLineEdit(centralwidget);
        Tree_View->setObjectName(QString::fromUtf8("Tree_View"));
        Tree_View->setGeometry(QRect(20, 80, 777, 281));
        MainWindow->setCentralWidget(centralwidget);
        menubar = new QMenuBar(MainWindow);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        menubar->setGeometry(QRect(0, 0, 823, 20));
        menu = new QMenu(menubar);
        menu->setObjectName(QString::fromUtf8("menu"));
        MainWindow->setMenuBar(menubar);
        statusbar = new QStatusBar(MainWindow);
        statusbar->setObjectName(QString::fromUtf8("statusbar"));
        MainWindow->setStatusBar(statusbar);

        menubar->addAction(menu->menuAction());
        menu->addSeparator();
        menu->addAction(action_2);
        menu->addAction(action_4);
        menu->addSeparator();
        menu->addAction(action_3);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QCoreApplication::translate("MainWindow", "MainWindow", nullptr));
        action_2->setText(QCoreApplication::translate("MainWindow", "\320\241\320\276\321\205\321\200\320\260\320\275\320\270\321\202\321\214", nullptr));
        action_3->setText(QCoreApplication::translate("MainWindow", "\320\222\321\213\320\271\321\202\320\270", nullptr));
        action_4->setText(QCoreApplication::translate("MainWindow", "\320\236\321\202\320\272\321\200\321\213\321\202\321\214", nullptr));
        Search_Button->setText(QCoreApplication::translate("MainWindow", "\320\237\320\276\320\270\321\201\320\272", nullptr));
        ProgramExit_Button->setText(QCoreApplication::translate("MainWindow", "\320\241\320\276\321\205\321\200\320\260\320\275\320\270\321\202\321\214 \320\270 \320\262\321\213\320\271\321\202\320\270", nullptr));
        TreeEdit_Button->setText(QCoreApplication::translate("MainWindow", "\320\240\320\265\320\264\320\260\320\272\321\202\320\270\321\200\320\276\320\262\320\260\321\202\321\214 \320\264\320\265\321\200\320\265\320\262\320\276", nullptr));
        Tree_Label->setText(QCoreApplication::translate("MainWindow", "\320\224\320\265\321\200\320\265\320\262\320\276:", nullptr));
        menu->setTitle(QCoreApplication::translate("MainWindow", "\320\234\320\265\320\275\321\216", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
