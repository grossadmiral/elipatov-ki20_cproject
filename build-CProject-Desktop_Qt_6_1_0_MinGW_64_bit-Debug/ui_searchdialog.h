/********************************************************************************
** Form generated from reading UI file 'searchdialog.ui'
**
** Created by: Qt User Interface Compiler version 6.1.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SEARCHDIALOG_H
#define UI_SEARCHDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPlainTextEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_SearchDialog
{
public:
    QWidget *horizontalLayoutWidget_2;
    QHBoxLayout *Exit_groupe;
    QSpacerItem *horizontalSpacer;
    QPushButton *Back_Button;
    QWidget *verticalLayoutWidget;
    QVBoxLayout *SearchResult_Groupe;
    QLabel *SearchResult_Label;
    QPlainTextEdit *SearchResult_View;

    void setupUi(QWidget *SearchDialog)
    {
        if (SearchDialog->objectName().isEmpty())
            SearchDialog->setObjectName(QString::fromUtf8("SearchDialog"));
        SearchDialog->resize(400, 257);
        horizontalLayoutWidget_2 = new QWidget(SearchDialog);
        horizontalLayoutWidget_2->setObjectName(QString::fromUtf8("horizontalLayoutWidget_2"));
        horizontalLayoutWidget_2->setGeometry(QRect(20, 199, 371, 41));
        Exit_groupe = new QHBoxLayout(horizontalLayoutWidget_2);
        Exit_groupe->setObjectName(QString::fromUtf8("Exit_groupe"));
        Exit_groupe->setContentsMargins(0, 0, 0, 0);
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        Exit_groupe->addItem(horizontalSpacer);

        Back_Button = new QPushButton(horizontalLayoutWidget_2);
        Back_Button->setObjectName(QString::fromUtf8("Back_Button"));

        Exit_groupe->addWidget(Back_Button);

        verticalLayoutWidget = new QWidget(SearchDialog);
        verticalLayoutWidget->setObjectName(QString::fromUtf8("verticalLayoutWidget"));
        verticalLayoutWidget->setGeometry(QRect(18, 20, 371, 171));
        SearchResult_Groupe = new QVBoxLayout(verticalLayoutWidget);
        SearchResult_Groupe->setObjectName(QString::fromUtf8("SearchResult_Groupe"));
        SearchResult_Groupe->setContentsMargins(0, 0, 0, 0);
        SearchResult_Label = new QLabel(verticalLayoutWidget);
        SearchResult_Label->setObjectName(QString::fromUtf8("SearchResult_Label"));

        SearchResult_Groupe->addWidget(SearchResult_Label);

        SearchResult_View = new QPlainTextEdit(verticalLayoutWidget);
        SearchResult_View->setObjectName(QString::fromUtf8("SearchResult_View"));

        SearchResult_Groupe->addWidget(SearchResult_View);


        retranslateUi(SearchDialog);

        QMetaObject::connectSlotsByName(SearchDialog);
    } // setupUi

    void retranslateUi(QWidget *SearchDialog)
    {
        SearchDialog->setWindowTitle(QCoreApplication::translate("SearchDialog", "Form", nullptr));
        Back_Button->setText(QCoreApplication::translate("SearchDialog", "\320\235\320\260\320\267\320\260\320\264", nullptr));
        SearchResult_Label->setText(QCoreApplication::translate("SearchDialog", "\320\240\320\265\320\267\321\203\320\273\321\214\321\202\320\260\321\202 \320\277\320\276\320\270\321\201\320\272\320\260:", nullptr));
    } // retranslateUi

};

namespace Ui {
    class SearchDialog: public Ui_SearchDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SEARCHDIALOG_H
