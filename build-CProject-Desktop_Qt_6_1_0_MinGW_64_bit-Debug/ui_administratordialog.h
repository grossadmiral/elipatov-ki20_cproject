/********************************************************************************
** Form generated from reading UI file 'administratordialog.ui'
**
** Created by: Qt User Interface Compiler version 6.1.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ADMINISTRATORDIALOG_H
#define UI_ADMINISTRATORDIALOG_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_AdministratorDialog
{
public:
    QAction *action;
    QAction *action_2;
    QAction *action_3;
    QWidget *centralwidget;
    QWidget *horizontalLayoutWidget_2;
    QHBoxLayout *Exit_Groupe;
    QSpacerItem *horizontalSpacer;
    QPushButton *ProgramExit_Button;
    QWidget *horizontalLayoutWidget_3;
    QHBoxLayout *UserEdit_Groupe;
    QSpacerItem *horizontalSpacer_2;
    QPushButton *UserEdit_Button;
    QLabel *User_Label;
    QLineEdit *UserView;
    QMenuBar *menubar;
    QMenu *menu;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *AdministratorDialog)
    {
        if (AdministratorDialog->objectName().isEmpty())
            AdministratorDialog->setObjectName(QString::fromUtf8("AdministratorDialog"));
        AdministratorDialog->resize(816, 446);
        action = new QAction(AdministratorDialog);
        action->setObjectName(QString::fromUtf8("action"));
        action_2 = new QAction(AdministratorDialog);
        action_2->setObjectName(QString::fromUtf8("action_2"));
        action_3 = new QAction(AdministratorDialog);
        action_3->setObjectName(QString::fromUtf8("action_3"));
        centralwidget = new QWidget(AdministratorDialog);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        horizontalLayoutWidget_2 = new QWidget(centralwidget);
        horizontalLayoutWidget_2->setObjectName(QString::fromUtf8("horizontalLayoutWidget_2"));
        horizontalLayoutWidget_2->setGeometry(QRect(10, 360, 779, 25));
        Exit_Groupe = new QHBoxLayout(horizontalLayoutWidget_2);
        Exit_Groupe->setObjectName(QString::fromUtf8("Exit_Groupe"));
        Exit_Groupe->setContentsMargins(0, 0, 0, 0);
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        Exit_Groupe->addItem(horizontalSpacer);

        ProgramExit_Button = new QPushButton(horizontalLayoutWidget_2);
        ProgramExit_Button->setObjectName(QString::fromUtf8("ProgramExit_Button"));

        Exit_Groupe->addWidget(ProgramExit_Button);

        horizontalLayoutWidget_3 = new QWidget(centralwidget);
        horizontalLayoutWidget_3->setObjectName(QString::fromUtf8("horizontalLayoutWidget_3"));
        horizontalLayoutWidget_3->setGeometry(QRect(10, 320, 781, 31));
        UserEdit_Groupe = new QHBoxLayout(horizontalLayoutWidget_3);
        UserEdit_Groupe->setObjectName(QString::fromUtf8("UserEdit_Groupe"));
        UserEdit_Groupe->setContentsMargins(0, 0, 0, 0);
        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        UserEdit_Groupe->addItem(horizontalSpacer_2);

        UserEdit_Button = new QPushButton(horizontalLayoutWidget_3);
        UserEdit_Button->setObjectName(QString::fromUtf8("UserEdit_Button"));

        UserEdit_Groupe->addWidget(UserEdit_Button);

        User_Label = new QLabel(centralwidget);
        User_Label->setObjectName(QString::fromUtf8("User_Label"));
        User_Label->setGeometry(QRect(10, 10, 91, 21));
        UserView = new QLineEdit(centralwidget);
        UserView->setObjectName(QString::fromUtf8("UserView"));
        UserView->setGeometry(QRect(10, 40, 781, 271));
        AdministratorDialog->setCentralWidget(centralwidget);
        menubar = new QMenuBar(AdministratorDialog);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        menubar->setGeometry(QRect(0, 0, 816, 20));
        menu = new QMenu(menubar);
        menu->setObjectName(QString::fromUtf8("menu"));
        AdministratorDialog->setMenuBar(menubar);
        statusbar = new QStatusBar(AdministratorDialog);
        statusbar->setObjectName(QString::fromUtf8("statusbar"));
        AdministratorDialog->setStatusBar(statusbar);

        menubar->addAction(menu->menuAction());
        menu->addAction(action);
        menu->addAction(action_2);
        menu->addSeparator();
        menu->addAction(action_3);

        retranslateUi(AdministratorDialog);

        QMetaObject::connectSlotsByName(AdministratorDialog);
    } // setupUi

    void retranslateUi(QMainWindow *AdministratorDialog)
    {
        AdministratorDialog->setWindowTitle(QCoreApplication::translate("AdministratorDialog", "MainWindow", nullptr));
        action->setText(QCoreApplication::translate("AdministratorDialog", "\320\241\320\276\321\205\321\200\320\260\320\275\320\270\321\202\321\214", nullptr));
        action_2->setText(QCoreApplication::translate("AdministratorDialog", "\320\236\321\202\320\272\321\200\321\213\321\202\321\214", nullptr));
        action_3->setText(QCoreApplication::translate("AdministratorDialog", "\320\222\321\213\320\271\321\202\320\270", nullptr));
        ProgramExit_Button->setText(QCoreApplication::translate("AdministratorDialog", "\320\241\320\276\321\205\321\200\320\260\320\275\320\270\321\202\321\214 \320\270 \320\262\321\213\320\271\321\202\320\270", nullptr));
        UserEdit_Button->setText(QCoreApplication::translate("AdministratorDialog", "\320\240\320\265\320\264\320\260\320\272\321\202\320\270\321\200\320\276\320\262\320\260\321\202\321\214 \320\277\320\276\320\273\321\214\320\267\320\276\320\262\320\260\321\202\320\265\320\273\321\217", nullptr));
        User_Label->setText(QCoreApplication::translate("AdministratorDialog", "\320\237\320\276\320\273\321\214\320\267\320\276\320\262\320\260\321\202\320\265\320\273\320\270:", nullptr));
        menu->setTitle(QCoreApplication::translate("AdministratorDialog", "\320\234\320\265\320\275\321\216", nullptr));
    } // retranslateUi

};

namespace Ui {
    class AdministratorDialog: public Ui_AdministratorDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ADMINISTRATORDIALOG_H
